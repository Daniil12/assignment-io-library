section .text

%define EXIT 60
%define STDOUT 1
%define WRITE 1
%define STDIN 0
%define READ 0

%define END_LINE 0x00
%define PROBEL 0x20
%define TAB 0x9
%define NEW_LINE 0xA
%define MINUS 0x2d
%define ZERO 0x30
%define NINE 0x39

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .next_iter:
        cmp byte [rdi + rax], END_LINE
        jnz .count
        ret
    .count:
        inc rax
        jmp .next_iter

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rsi, rdi
    mov rdx, rax
    mov rax, WRITE
    mov rdi, STDOUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rax, WRITE
    mov rdi, STDOUT
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, END_LINE

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rdi, 10
    push END_LINE
.iter:
    xor rdx, rdx
    div rdi
    add rdx, '0'
    push rdx
    cmp rax, END_LINE
    jnz .iter
.end:
    pop rdi
    cmp rdi, END_LINE
    jnz .write
    ret
.write:
    call print_char
    jmp .end


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, END_LINE
    jns print_uint
    push rdi
    mov rdi, MINUS
    call print_char
    pop rdi
    neg rdi
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    call string_length
    mov rdx, rax
    call string_length
    cmp rax, rdx
    jne .false
    xor rax, rax
    .comp_iter:
    	mov bl, byte [rdi + rax]
    	cmp bl, byte [rsi + rax]
    	jne .false
    	cmp rax, rdx
    	jz .end
    	inc rax
    	jmp .comp_iter
.false:
    xor rax, rax
    ret
.end:
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rdx, 1
    push END_LINE
    mov rsi, rsp
    mov rax, READ
    mov rdi, STDIN
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    mov r8, rdi
    mov r9, rsi
    xor r10, r10
    .skip:
    	call read_char
    	call check
    	jz .skip
    .loop:
 	cmp rax, END_LINE
    	jz .end
    	call check
    	jz .end
    	cmp r10, r9
    	je .false
    	mov [r8 + r10], rax
    	inc r10
    	call read_char
    	jmp .loop
    .end:
    	mov [r8 + r10], byte END_LINE
    	mov rax, r8
    	mov rdx, r10
    	ret
    .false:
    	xor rax, rax
    	ret    
    	

check:
    cmp al, PROBEL
    jz .end
    cmp al, TAB
    jz .end
    cmp al, NEW_LINE
    jz .end
.end:
    ret
    	
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rcx, rcx
    xor rdx, rdx
    xor rax, rax
.iter:
    mov cl, byte [rdi + rdx]
    cmp cl, END_LINE
    jz .end
    cmp cl, ZERO
    jl .end
    cmp cl, NINE
    jg .end
    sub cl, ZERO
    imul rax, rax, 10
    add rax, rcx
    inc rdx
    jmp .iter
.end:
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [edi], MINUS
    je .int
    call parse_uint
    ret
.int:
    inc rdi
    call parse_uint
     neg rax
    inc rdx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length
    inc rax
    cmp rax, rdx
    ja .false
    xor rax, rax
    .copy:
        mov bl, byte [rdi + rax]
        mov byte [rsi + rax], bl
    	cmp bl, END_LINE
    	jnz .count
    	ret
    .count:
    	inc rax
    	jmp .copy
    .false:
    	xor rax, rax
    	ret

